const version = '20181209125900';

const down = () => {
    // We don't want to necessarily lose our polling data.
    return [];
};

const up = () => {
    return [
	'CREATE TABLE IF NOT EXISTS poll (id SERIAL PRIMARY KEY, data JSONB);',
    ];
};

module.exports = {
    down,
    up,
    version,
};
