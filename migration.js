const pgp = require('pg-promise')();
const db_user = process.env.DATABASE_USER || 'postgres';
const db_password = process.env.DATABASE_PASSWORD ? `:${process.env.DATABASE_PASSWORD}` : '';
const db_name = process.env.DATABASE_NAME || 'postgres';
const db_host = process.env.DATABASE_HOST || 'db:5432';
const db = pgp(`postgres://${db_user}${db_password}@${db_host}/${db_name}`);

const migrations = () => {
    return [
	require('./migrations/20181209125900_init_db'),
    ].sort((a, b) => parseInt(a.version, 10) > parseInt(b.version, 10));
};

const current = () => {
    return db.one('SELECT * FROM migration ORDER BY applied DESC;').then((data) => {
	return data.version;
    }).catch(() => {
	return null;
    });
};

const set = (version) => {
    return new Promise((resolve) => {
	return db.none('INSERT INTO migration (version, applied) VALUES ($1, CURRENT_TIMESTAMP)', version).then(() => {
	    resolve();
	}).catch(() => {
	    resolve();
	});
    });
};

const ensure = () => {
    return db.none('CREATE TABLE IF NOT EXISTS migration (version VARCHAR(200), applied TIMESTAMP, UNIQUE (version));');
};

const up = () => {
    const targets = migrations();
    return ensure().then(current).then((version) => {
	return targets.reduce((arr, value) => {
	    if (parseInt(value.version, 10) > parseInt(version || 0, 10)) {
		value.up().forEach(sql => arr.push(sql));
	    }

	    return arr;
	}, []).filter(x => x !== null && x !== '');
    }).then((statements) => {
	if (statements.length === 0) return null;
	return db.none(statements.join(';'));
    }).then(() => {
	return set(targets[targets.length - 1].version);
    });
};

module.exports = {
    up,
};
