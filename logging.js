const express = require('express');
const router = express.Router();

const log = (req, res, next) => {
    console.log('-- begin');
    console.log('\nRequest (params): ', req.params);
    console.log('\nRequest (query): ', req.query);
    console.log('\nSession: ', req.session);
    console.log('-- end');
    next();
};

// Apply middleware
router.use(log);

module.exports = router;
