const request = require('request');
const express = require('express');
const router = express.Router();

const api_endpoint = process.env.AUTHENTICATION_ENDPOINT || 'https://discordapp.com/api';
const client_id = process.env.CLIENT_ID || '';
const client_secret = process.env.CLIENT_SECRET || '';
const client_scope = process.env.CLIENT_SCOPE || 'identify email';
const state = process.env.STATE || 'thestate';
const redirect_uri = process.env.REDIRECT_URI || 'http://127.0.0.1:3000/auth/callback';

const getToken = (form) => {
    const headers = {
	'Content-Type': 'application/x-www-form-urlencoded',
    };

    const options = {
	url: `${api_endpoint}/oauth2/token`,
	headers,
	method: 'POST',
	form,
    };

    return new Promise((resolve, reject) => {
	console.log('Getting token with these options: ', options);
	request.post(options, (err, response, body) => {
	    console.log('Token response: ', body);
	    resolve(JSON.parse(body));
	});
    });
};

const handleToken = (req, res, token) => {
    if (!(token || {}).access_token) {
	req.session.token = null;
	res.redirect('/auth/access_denied');
	return;
    }

    const now = new Date();
    token.expires_at = now.setSeconds(now.getSeconds() + token.expires_in);
    req.session.token = token;
    res.redirect('/');
};

router.get('/auth/sign_in', (req, res) => {
    const token = req.session.token;
    if (token) {
	console.log('Found a token: ', token);
	
	// If we need to refresh the token
	const now = new Date().getTime();
	const diff = token.expires_at - now;
	if (diff < 120) {
	    console.log('Token needs to be refreshed');
	    res.redirect('/auth/refresh');
	}

	// Else we continue on our merry way
	console.log('Token is okay');
	res.redirect('/');
    }

    // Redirect to OAuth2 authentication flow.
    console.log('No token found need to request one');
    res.redirect(`${api_endpoint}/oauth2/authorize?response_type=code&client_id=${client_id}&scope=${client_scope}&state=${state}&redirect_uri=${redirect_uri}`);
});

router.get('/auth/callback', (req, res) => {
    const {code, state} = req.query;
    const form = {
	client_id,
	client_secret,
	grant_type: 'authorization_code',
	code,
	redirect_uri,
	scope: client_scope,
    };

    getToken(form).then((token) => {
	handleToken(req, res, token);
    });
});

router.get('/auth/refresh', (req, res) => {
    const token = req.session.token;
    const form = {
	client_id,
	client_secret,
	grant_type: 'refresh_token',
	refresh_token: token.refresh_token,
	redirect_uri,
	scope: client_scope,
    };

    getToken(form).then((token) => {
	handleToken(req, res, token);
    });
});

module.exports = router;
