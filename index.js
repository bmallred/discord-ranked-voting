const express = require('express');
const session = require('express-session');
const logging = require('./logging');
const oauth = require('./oauth');
const migration = require('./migration');
const identify = require('./identify');
const app = express();
const port = 3000;

// Declare session settings.
let sess = {
    secret: process.env.SESSION_SECRET || 'my voice is my passphrase',
    cookie: {},
};

// In production we want to tighten up the security.
if (app.get('env') === 'production') {
    app.set('trust proxy', 1);
    sess.cookie.secure = true;
}

// Apply middleware
app.use(session(sess), logging, oauth);

// Simple index handling
app.get('/', identify, (req, res) => {
    if (!req.session.token) {
    	res.redirect('/auth/sign_in');
    }

    res.send('Hello world');
});

// Sync database to the most current
migration.up().then(() => {
    // Listen to the port.
    app.listen(port, () => console.log(`Example app listening on port ${port}!`));
}).catch((err) => {
    console.log(`database error: ${err}`);
});
