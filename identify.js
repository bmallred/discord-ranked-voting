const request = require('request');
const express = require('express');
const router = express.Router();
const api_endpoint = process.env.AUTHENTICATION_ENDPOINT || 'https://discordapp.com/api';

const identify = (req, res, next) => {
    const access_token = (req.session.token || {}).access_token;
    if (!access_token) return next();

    const headers = {
    	'Authorization': `Bearer ${access_token}`,
    };

    const options = {
    	url: `${api_endpoint}/users/@me`,
    	headers,
    	method: 'GET',
    };

    return request.get(options, (err, response, body) => {
    	if (err) {
    	    req.session.user = null;
    	    return next();
    	}

	const user = JSON.parse(body);
	if (!user.email) {
    	    req.session.user = null;
    	    return next();
	}

    	req.session.user = user;
    	return next();
    });
};

// Apply middleware
router.use(identify);

module.exports = router;
